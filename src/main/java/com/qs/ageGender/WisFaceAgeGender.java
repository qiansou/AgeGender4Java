package com.qs.ageGender;

import java.awt.image.BufferedImage;
import java.io.IOException;

import com.qs.Util;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Structure;


/**
 * 年龄、性别识别
 * 
 * @author ppf@jiumao.org
 * @date 2017年11月8日
 */
public class WisFaceAgeGender {

    private long handler = 0;


    /**
     * <code>
     * AgeGender a = ...;<br>
     * a.init();<br>
     * a.process(...);<br>
     * </code>
     * <p>
     * 配合 {@link #process(String, int)}使用，自动释放句柄。
     * <p>
     * 手动释放调用{@link #createExtend()}
     * 
     * @return 大于0的值，否则初始化失败
     */
    public long init() {
        this.handler = createExtend();
        return handler;
    }


    public static void main(String[] args) {
        WisFaceAgeGender t = new WisFaceAgeGender();
        t.init();
        QsFaceAttr[] attrs = t.process("1.jpg", 1);
        for (QsFaceAttr attr : attrs) {
            System.out.println("age:" + attr.age + " | gender:" + (attr.gender == 1 ? "男" : "女"));
        }
    }


    public QsFaceAttr[] process(String imgPath, int maxFaceNum) {
        QsRect[] rects = new QsRect[maxFaceNum];
        QsFaceAttr.ByReference[] attrs = new QsFaceAttr.ByReference[maxFaceNum];

        try {
            BufferedImage img = Util.toBufferedImage(imgPath);
            int faceNum = processDetect(this.handler, Util.toBytes(img), img.getWidth(), img.getHeight(), img.getWidth() * 3, rects,
                    rects.length);
            System.out.println(faceNum);
            for (int i = 0; i < faceNum; i++) {
                attrs[i] = new QsFaceAttr.ByReference();
                processFaceInfo(this.handler, Util.toBytes(img), img.getWidth(), img.getHeight(), img.getWidth() * 3,
                        rects[i].asValue(), attrs[i]);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            processDispose(handler);
        }

        return attrs;
    }

    public static class QsRect extends DefaultStruct {
        public int left; // 矩形框左上角x坐标
        public int top; // 矩形框左上角y坐标
        public int right; // 矩形框右下角x坐标
        public int bottom; // 矩形框右下角y坐标

        public static class ByValue extends QsRect implements Structure.ByValue {}


        public QsRect.ByValue asValue() {
            ByValue val = new ByValue();
            val.left = this.left;
            val.top = this.top;
            val.right = this.right;
            val.bottom = this.bottom;
            return val;
        }
    }

    public static class QsFaceAttr extends DefaultStruct {
        public int gender; // 1 man, -1 woman
        public float age;

        public static class ByReference extends QsFaceAttr implements Structure.ByReference {}


        public QsFaceAttr.ByReference asReference() {
            QsFaceAttr.ByReference ref = new ByReference();
            ref.gender = this.gender;
            ref.age = this.age;
            return ref;
        }
    }


    /**
     * 创建人脸扩展引擎
     * 
     * @return 返回人脸引擎扩展句柄engine if >0 sucess, or falied
     */
    public static long createExtend() {
        return WisFaceEngineExtendLibrary.INSTANCE.qs_Wis_Create_Extend();
    }


    /**
     * 人脸检测
     * 
     * @param engine qs_Wis_Create_Extend()返回的人脸引擎句柄
     * @param image 照片imgBgr24
     * @param width 图像宽度
     * @param height 图像高度
     * @param widthstep widthstep是存储一行图像所占的字节（相邻两行起点指针的差值）
     * @param faceRects 返回人脸位置
     * @param maxFaceNum faceRects 的数组长度
     * 
     * @param handler
     * @return 检测到的人脸个数
     */
    public static int processDetect(long handler, byte[] imgBgr24, int width, int height, int widthstep, QsRect[] faceRects, int maxFaceNum) {
        return WisFaceEngineExtendLibrary.INSTANCE.qs_Wis_Process_Detect(handler, imgBgr24, width, height, widthstep, faceRects,
                maxFaceNum);
    }


    /**
     * 
     * @param handler 返回的人脸引擎句柄
     * @param imgBgr24 照片imgBgr24
     * @param width 图像宽度
     * @param height 图像高度
     * @param widthstep widthstep是存储一行图像所占的字节（相邻两行起点指针的差值）
     * @param faceRect 输入人脸的位置，调用qs_Wis_DetectFaces，返回人脸QsFace.rect
     * @param attr 返回人脸属性
     * @return 0 success
     */
    public static int processFaceInfo(long handler, byte[] imgBgr24, int width, int height, int widthstep, QsRect.ByValue faceRect,
            QsFaceAttr.ByReference attr) {
        return WisFaceEngineExtendLibrary.INSTANCE.qs_Wis_Process_FaceInfo(handler, imgBgr24, width, height, widthstep, faceRect, attr);
    }


    /**
     * 释放资源
     * 
     * @param handler
     */
    public static void processDispose(long handler) {
        WisFaceEngineExtendLibrary.INSTANCE.qs_Wis_Process_Dispose(handler);
    }

    public interface WisFaceEngineExtendLibrary extends Library {
        WisFaceEngineExtendLibrary INSTANCE =
                (WisFaceEngineExtendLibrary) Native.loadLibrary("WisFaceEngineExtend", WisFaceEngineExtendLibrary.class);


        long qs_Wis_Create_Extend();


        int qs_Wis_Process_Detect(long handler, byte[] imgBgr24, int width, int height, int widthstep, QsRect[] faceRects, int maxFaceNum);


        int qs_Wis_Process_FaceInfo(long handler, byte[] imgBgr24, int width, int height, int widthstep, QsRect.ByValue faceRect,
                QsFaceAttr.ByReference attr);


        void qs_Wis_Process_Dispose(long handler);
    }

}
