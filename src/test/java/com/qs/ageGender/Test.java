package com.qs.ageGender;

import java.awt.image.BufferedImage;

import com.qs.Util;
import com.qs.ageGender.WisFaceAgeGender.QsFaceAttr;
import com.qs.ageGender.WisFaceAgeGender.QsRect;
import com.qs.facePose.WisFacePose;
import com.qs.facePose.WisFacePose.FacePointsModel;
import com.qs.facePose.WisFacePose.POINT;
import com.qs.facePose.WisFacePose.RECT;


public class Test {

    @org.junit.Test
    public void testAgeGender() throws Exception {
        long engine = WisFaceAgeGender.createExtend();
        BufferedImage img = Util.toBufferedImage("1.jpg");
        int faceMaxCount = 10;
        QsRect[] faceRects = new QsRect[faceMaxCount];
        int faceNum = WisFaceAgeGender.processDetect(engine, Util.toBytes(img), img.getWidth(), img.getHeight(), img.getWidth() * 3, faceRects, 1);

        if (faceNum < 1) {
            System.out.println("no face in 1.jpg");
            return;
        }
        QsFaceAttr.ByReference attr = new QsFaceAttr.ByReference();
        for (int i = 0; i < faceRects.length && i < faceNum; i++) {
            QsRect.ByValue rect = faceRects[i].asValue();
            WisFaceAgeGender.processFaceInfo(engine, Util.toBytes(img), img.getWidth(), img.getHeight(), img.getWidth() * 3, rect, attr);

            System.out.println("age:" + attr.age + " | gender:" + attr.gender);
        }

        WisFaceAgeGender.processDispose(engine);
    }


    @org.junit.Test
    public void testFasePose() throws Exception {

        int detectEngine = WisFacePose.createFacePointsEngine(null);
        System.out.println(detectEngine);
        RECT[] rtFaces = new RECT[10];
        BufferedImage img = Util.toBufferedImage("1.jpg");
        int facenum = WisFacePose.detectFaces(Util.toBytes(img), img.getWidth(), img.getHeight(), img.getWidth() * 3, rtFaces);

        System.out.println("facenum:" + facenum);

        // 绘制图片和图片上的标注点
        for (int i = 0; i < facenum; i++) {
            FacePointsModel.ByReference fpm = new FacePointsModel.ByReference();
            float score = WisFacePose.calculateFacePoints(detectEngine, Util.toBytes(img), img.getWidth(), img.getHeight(),
                    img.getWidth() * 3, rtFaces[i].asValue(), fpm);
            System.out.println("score:" + score);
            System.out.println("Three angles :" + fpm);
            System.out.println("51 points:");
            for (POINT point : fpm.points) {
                System.out.println(point);
            }
        }
    }

}
